import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Listeners;

import static com.codeborne.selenide.Configuration.browser;

@Listeners({Listener.class})
public class Test {

    @BeforeSuite
    public void setUp() {
        browser = "CHROME";
    }

    @org.testng.annotations.Test(groups = {"GROUP"}, priority = 1)
    @Severity(SeverityLevel.BLOCKER)
    public void nameValidTest() throws Exception {
        Base.openHomepage();
        Base.searchFor("Set");
        Base.clickSearch();
        Base.isFirstElementHas("Set");
    }

    @org.testng.annotations.Test(groups = {"GROUP"})
    @Severity(SeverityLevel.BLOCKER)
    public void nameInvalidTest() throws Exception {
        Base.openHomepage();
        Base.searchFor("One");
        Base.clickSearch();
        Base.isFirstElementHas("Set");
    }
}
