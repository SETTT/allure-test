import io.qameta.allure.Step;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.*;

public class Base {

    @Step ("Open homepage")
    public static void openHomepage(){
        open("https://www.google.com");
        sleep(2000);
    }

    @Step ("Click search")
    public static void clickSearch(){
        $("#lst-ib").submit();
    }

    @Step ("Search for {value}")
    public static void searchFor(String value){
        $("#lst-ib").setValue(value);
    }

    @Step ("First element contains {value}")
    public static void isFirstElementHas(String value){
        $x("//h3[contains(@class,'r')]").shouldHave(text(value));
    }

}
